#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void hello()
{
    cout << "Hello from thread ";
    cout << this_thread::get_id()  << endl;
    this_thread::sleep_for(chrono::seconds(1));
    cout << "Task finished" << endl;
}

void hello2(int id)
{
    cout << "Hello from parametrized thread ";
    cout << id << endl;
    this_thread::sleep_for(chrono::seconds(1));
    cout << "Task finished" << endl;
}

void answer(int &ret)
{
    ret = 42;
}

struct Callable
{
    void operator()(int id)
    {
        cout << "Hello from Callable thread ";
        cout << id << endl;
        this_thread::sleep_for(chrono::seconds(1));
        cout << "Task finished" << endl;
    }
};

class BackgroundTask
{
    vector<int> buff;
public:
    BackgroundTask() {}
    void assign(const vector<int>& src)
    {
        buff.assign(src.begin(), src.end());
    }
    vector<int> data() const
    {
        return buff;
    }
};

int main()
{
    thread th1(&hello);
    thread th2(&hello2, 21);
    int res{};
    thread th3(&answer, ref(res));
    Callable cal;
    thread th4([] { cout << "Lambda is called" << endl;} );
    thread th5(cal, 33);
    BackgroundTask bt;
    vector<int> v{1,2,3,4,5};
    thread th6(&BackgroundTask::assign, &bt, cref(v));
    thread th7([&] { bt.assign(v); });
    th1.join();
    th2.join();
    th3.join();
    th4.join();
    th5.join();
    th6.join();
    th7.join();
    for (auto& el : bt.data())
        cout << el << "; ";
    cout << endl;
    cout << "RES: " << res << endl;
    return 0;
}

