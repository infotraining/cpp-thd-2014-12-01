#include <iostream>
#include <vector>
#include <thread>
#include <atomic>
#include <mutex>

using namespace std;

atomic<long> counter{0};
//long counter{0};

mutex mtx;

class spinlock
{
    atomic_flag flag;
public:
    spinlock() : flag(ATOMIC_FLAG_INIT) {}
    bool try_lock()
    {
        return !flag.test_and_set();
    }

    void lock()
    {
        while(!flag.test_and_set());
    }

    void unlock()
    {
        flag.clear();
    }
};


class ScopedThread
{
    thread th_;
public:
    ScopedThread(thread&& th) : th_(move(th)) {}

    template<typename ...T>
    ScopedThread(T&&... args) : th_(forward<T>(args)...) {}

    // non-copyable
    ScopedThread(const ScopedThread&) = delete;
    ScopedThread& operator=(const ScopedThread&) = delete;

    //default move opeations
    ScopedThread(ScopedThread&&) = default;
    ScopedThread& operator=(ScopedThread&&) = default;

    ~ScopedThread()
    {
        if (th_.joinable())
            th_.join();
    }
};

class LockGuard
{
    // RAII for mutex;
    mutex& mtx;
public:
    LockGuard(const LockGuard&) = delete;
    LockGuard& operator=(const LockGuard&) = delete;
    LockGuard(mutex& mtx) : mtx(mtx)
    {
        mtx.lock();
    }
    ~LockGuard()
    {
        mtx.unlock();
    }
};

spinlock mts;

void increment()
{
    for (int i = 0  ; i < 100000 ; ++i)
    {
        //__sync_fetch_and_add(&counter, 1);
        //counter.fetch_add(1, memory_order_relaxed);

        //lock_guard<spinlock> lk(mts);
        //lock_guard<mutex> lk(mtx);
        ++counter;
        //        if (counter == 1000)
        //        {
        //            return;
        //        }
    }
}

int main()
{
    atomic<double> dlb;
    cout << "Is double lock_free? " << dlb.is_lock_free() << endl;
    auto start = chrono::high_resolution_clock::now();
    {
        vector<ScopedThread> thds;
        for(int i = 0 ; i < 16 ; ++i)
        {
            thds.emplace_back(increment);
        }
        //for (auto& th : thds) th.join();
    }
    cout << "C = " << counter << endl;
    auto end = chrono::high_resolution_clock::now();

    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
    return 0;
}

