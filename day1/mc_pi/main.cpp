#include <random>
#include <iostream>
#include <chrono>
#include <thread>

using namespace std;

void pi_calc(long N, long& cnt)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(0, 1);
    double x, y;
    for (long n = 0; n < N; ++n) {
        x = dis(gen);
        y = dis(gen);
        if(x*x + y*y < 1)
            ++cnt;
    }
}

int main()
{
    cout << "HC = " << thread::hardware_concurrency() << endl;
    int thread_count = thread::hardware_concurrency();
    thread_count = 4;
    const long N = 100*1000*1000;
    vector<long> counters(1);
    vector<thread> threads;
    auto start = chrono::high_resolution_clock::now();
    for(int i = 0 ; i < thread_count ; ++i)
    {
        threads.emplace_back(pi_calc, N/thread_count, ref(counters[i]));
    }
    for(auto& th : threads) th.join();
    auto end = chrono::high_resolution_clock::now();
    double hits = accumulate(counters.begin(), counters.end(), 0L);
    cout << "Pi = " << double(hits)/double(N)*4 << endl;
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
}
