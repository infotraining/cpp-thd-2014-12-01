#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void hello()
{
//    cout << "hello" << endl;
    //this_thread::sleep_for(chrono::milliseconds(100));
}

void hello2(int id)
{
    cout << "Hello from parametrized thread ";
    cout << id << endl;
    this_thread::sleep_for(chrono::seconds(1));
    cout << "Task finished" << endl;
}

void answer(int &ret)
{
    ret = 42;
}

thread generate_thread()
{
    int ans;
    return thread(&answer, ref(ans));// wrong, danglin ref
}

int main()
{
    vector<thread> threads;
    thread th1(hello);
    //thread th2 = generate_thread();

    //threads.push_back(move(th2));
    threads.push_back(generate_thread());
    //threads.push_back(thread(hello));
    threads.emplace_back(&hello2, 10);

    this_thread::sleep_for(chrono::milliseconds(100));
    cout << "Joinable?: " <<  th1.joinable() << endl;
    th1.join();
    cout << "Joinable?: " <<  th1.joinable() << endl;
    //th2.join();
    for( auto& th : threads) th.join();
    return 0;
}

