#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include "../../utils.h"
#include <atomic>

using namespace std;



class spinlock
{
    atomic_flag flag;
public:
    spinlock() : flag(ATOMIC_FLAG_INIT) {}
    bool try_lock()
    {
        return !flag.test_and_set();
    }

    void lock()
    {
        while(!flag.test_and_set());
    }

    void unlock()
    {
        flag.clear();
    }
};

//timed_mutex mtx;
spinlock mtx;

void worker()
{
    cout << "Worker started " << this_thread::get_id() << endl;
    unique_lock<spinlock> ul(mtx, try_to_lock);
    if (!ul.owns_lock())
    {
        do
        {
            cout << "Waiting for mutex " << this_thread::get_id() << endl;
            this_thread::sleep_for(chrono::milliseconds(200));
        }
        while(!ul.try_lock());
    }
    cout << "Got lock! " << this_thread::get_id() << endl;
    this_thread::sleep_for(chrono::seconds(1));
}

int main()
{
    vector<scoped_thread> thds;
    thds.emplace_back(worker);
    thds.emplace_back(worker);
    return 0;
}

