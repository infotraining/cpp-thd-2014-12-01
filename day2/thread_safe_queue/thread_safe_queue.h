#ifndef THREAD_SAFE_QUEUE_H
#define THREAD_SAFE_QUEUE_H

#include <queue>
#include <condition_variable>
#include <mutex>

template<typename T>
class thread_safe_queue
{
    std::queue<T> q;
    std::mutex qmtx;
    std::condition_variable qcond_out;
    std::condition_variable qcond_in;
    size_t size_limit;

public:
    thread_safe_queue(size_t limit) : size_limit(limit)
    {

    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> l(qmtx);
        qcond_out.wait(l, [this] {return !q.empty();});
        item = q.front();
        q.pop();
        qcond_in.notify_one();
    }

    void push(T item)
    {
        std::unique_lock<std::mutex> l(qmtx);
        while (q.size() >= size_limit)
            qcond_in.wait(l);
        q.push(item);
        qcond_out.notify_one();
    }
};

#endif // THREAD_SAFE_QUEUE_H
