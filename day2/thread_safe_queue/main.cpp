#include <iostream>
#include "../../utils.h"
#include "thread_safe_queue.h"


using namespace std;

thread_safe_queue<int> q(10);

void producer()
{
    for(int i = 0 ; i < 100;++i)
    {
        q.push(i);
    }
    q.push(-1);
}

void consumer(int id)
{
    for(;;)
    {
        this_thread::sleep_for(chrono::milliseconds(20));
        int msg;
        q.pop(msg);
        if(msg == -1)
        {
            cout << "consumer " << id << " finishing" << endl;
            q.push(-1);
            return;
        }
        cout << "prod " << id <<
                " got " << msg  << endl;
    }
}

int main()
{
    cout << "TSQ" << endl;
    scoped_thread th_prod(producer);
    scoped_thread th_cons1(consumer, 1);
    scoped_thread th_cons2(consumer, 2);
    return 0;
}

