#include <iostream>
#include <vector>
#include "../../utils.h"
#include <thread>
#include <atomic>
#include <algorithm>
#include <condition_variable>

using namespace std;

namespace atom
{
class Data
{
    vector<int> data_;
    atomic<bool> is_ready;
public:
    Data() : is_ready(false) {}

    void read()
    {
        cout << "Reading data" << endl;
        this_thread::sleep_for(chrono::milliseconds(10000));
        data_.resize(10);
        generate(data_.begin(), data_.end(), [] {return rand() % 100 ;});
        is_ready = true;
        cout << "end reading" << endl;
    }

    void process()
    {
        for(;;)
        {
            if(is_ready)
            {
                cout << "processing" << endl;
                long sum = accumulate(data_.begin(), data_.end(), 0L);
                cout << "Sum = " << sum << endl;
                return;
            }
            //this_thread::sleep_for(chrono::microseconds(1));
            this_thread::yield();
        }
    }
};
}

namespace cond
{
class Data
{
    vector<int> data_;
    mutex mtx;
    condition_variable cond;
    bool is_ready;

public:
    Data() : is_ready(false) {}

    void read()
    {
        cout << "Reading data" << endl;
        this_thread::sleep_for(chrono::milliseconds(100));
        data_.resize(10);
        generate(data_.begin(), data_.end(), [] {return rand() % 100 ;});
        {
            lock_guard<mutex> l(mtx);
            is_ready = true;
        }
        cond.notify_all();
        cout << "end reading" << endl;
    }

    void process()
    {
        unique_lock<mutex> l(mtx);
        cond.wait(l, [this] {return is_ready;});
        cout << "processing" << endl;
        long sum = accumulate(data_.begin(), data_.end(), 0L);
        cout << "Sum = " << sum << endl;
    }
};
}


using namespace cond;

int main()
{
    Data data;
    scoped_thread th_process1([&data] { data.process();});
    scoped_thread th_process2([&data] { data.process();});
    scoped_thread th_read([&data] { data.read();});
    return 0;
}

