#include <iostream>
#include <vector>
#include "../../utils.h"
#include <thread>
#include <atomic>
#include <algorithm>
#include <condition_variable>
#include <queue>

using namespace std;

queue<int> q;
mutex qmtx;
condition_variable qcond;

void producer()
{
    for (int i = 0 ; i < 100 ; ++i)
    {
        lock_guard<mutex> l(qmtx);
        q.push(i);
        qcond.notify_one();
    }
    q.push(-1);
    qcond.notify_all();
}

void consumer(int id)
{
    for(;;)
    {
        {
        unique_lock<mutex> l(qmtx);
        qcond.wait(l, [] {return !q.empty();});
        int msg = q.front();
            if (msg == -1) return;
        cout << "prod " << id << " just got "
             << msg  << endl;
        q.pop();
        }
        this_thread::sleep_for(chrono::milliseconds(id*20));
    }
}

int main()
{
    cout << "PRod cons" << endl;
    scoped_thread th_prod(producer);
    scoped_thread th_cons1(consumer, 1);
    scoped_thread th_cons2(consumer, 2);
    return 0;
}

