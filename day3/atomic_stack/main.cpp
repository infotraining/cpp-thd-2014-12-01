#include <iostream>
#include <mutex>
#include "../../utils.h"
#include <atomic>

using namespace std;

struct Node
{
    Node* prev;
    int data;
};

class Stack
{
    atomic<Node*> head;
public:
    Stack() : head(nullptr) {}

    void push(int item)
    {
        Node* new_node = new Node;
        new_node->prev = head.load();
        new_node->data = item;
        //head.store(new_node); // still race...
        while(!head.compare_exchange_weak(new_node->prev, new_node));
    }

    int pop()
    {
        Node* old_node = head.load();
        while(!head.compare_exchange_weak(old_node, old_node->prev));
        int res = old_node->data;
        //delete old_node;
        return res;
    }
};

mutex cmutex;

void push_and_pop(Stack& s, int start)
{
    for (int i = start ; i < start + 1000 ; ++i)
        s.push(i);

    for (int i = start ; i < start + 1000 ; ++i)
    {
        //lock_guard<mutex> l(cmutex);
        cout << s.pop() << " ; ";
    }
}

int main()
{
    Stack s;
    scoped_thread th1(push_and_pop, ref(s), 0);
    scoped_thread th2(push_and_pop, ref(s), 1000);
    return 0;
}

