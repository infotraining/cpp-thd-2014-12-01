#include <iostream>
#include <functional>
#include "../../utils.h"
#include "thread_pool.h"

using namespace std;

void hello(int id)
{
    cout << "Function " << id << endl;
}

void task()
{
    cout << "Task called"<< endl;
}

int main()
{
    cout << "Hello Thread Pool!" << endl;
    std::function<void(int)> f;
    f = [] (int id) { cout << "Lambda " << id << endl;};
    f(12);
    f = hello;
    f(20);

    thread_pool tp(4);
    tp.send(task);   // accepts std::function<void(void)>
    tp.send([] { cout << "Lambda" << endl;});
    for(int i = 0 ; i < 20 ; ++i)
    {
        tp.send( [i]
        {
           this_thread::sleep_for(chrono::milliseconds(rand() % 100));
           cout << "Lambda id = " << i << endl;
        });
    }

    future<int> res = tp.async([] {return 42;});
    cout << "REs = " << res.get() << endl;

    return 0;
}

