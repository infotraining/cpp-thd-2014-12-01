#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include <functional>
#include <thread>
#include <vector>
#include <future>
#include "../../utils.h"
#include "../../day2/thread_safe_queue/thread_safe_queue.h"

using task_t = std::function<void(void)>;

class thread_pool
{
    thread_safe_queue<task_t> task_queue;
    std::vector<std::thread> workers;

    void worker()
    {
        for(;;)
        {
            task_t task;
            task_queue.pop(task);
            if (!task)
                return;
            task();
        }
    }

public:
    thread_pool(int pool_size) : task_queue(1024)
    {
        for( int i = 0 ; i < pool_size ; ++i)
        {
            workers.emplace_back(&thread_pool::worker, this);
        }
    }

    void send(task_t task)
    {
        task_queue.push(task);
    }

    template<typename F>
    auto async(F fun) -> std::future<decltype(fun())>
    {
        using Res = decltype(fun());
        auto task = std::make_shared<std::packaged_task<Res()>>(fun); // allocation...
        std::future<Res> res = task->get_future();
        task_queue.push( [task] { (*task)(); });
        return res;
    }

    ~thread_pool()
    {
        for(auto& th : workers)
            task_queue.push(task_t());
        for(auto& th : workers)
            th.join();
    }
};

#endif // THREAD_POOL_H
