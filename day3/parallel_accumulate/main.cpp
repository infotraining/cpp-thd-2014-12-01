#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include "../../utils.h"
#include <cstdint>
#include <future>

using namespace std;

template<typename It, typename T>
T parallel_accumulate(It begin, It end, T init)
{
    int N = max(thread::hardware_concurrency(), 1u);
    vector<future<T>> results;
    size_t block_size = distance(begin, end)/N;

    It block_start = begin;

    for (int i = 0 ; i < N ; ++i)
    {
        It block_end = block_start;
        advance(block_end, block_size);
        if( i == N-1 ) block_end = end;
        results.push_back(async(launch::async,
                            accumulate<It, T>,
                            block_start, block_end, T()));
        block_start = block_end;
    }

    //sum futures
//    T sum{};
//    for (auto& f : results)
//        sum += f.get();

    return accumulate(results.begin(), results.end(), init,
                      [] (T a, future<T>& b){ return a + b.get();});
}

int main()
{
    const size_t N = 100000000;

    vector<long> v;
    v.reserve(N);
    {
        Timer<chrono::milliseconds> t("creating");
        for (long i = 0 ; i < N ; ++i)
            v.push_back(i);
    }
    {
        Timer<chrono::milliseconds> t("accumulate");
        cout << accumulate(v.begin(), v.end(), 0L) << endl;
    }
    {
        Timer<chrono::milliseconds> t("parallel accumulate");
        cout << parallel_accumulate(v.begin(), v.end(), 0L) << endl;
    }
    return 0;
}

