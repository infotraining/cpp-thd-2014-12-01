#include <iostream>
#include <future>
#include "../../utils.h"
#include <stdexcept>

using namespace std;

int answer(int id)
{
    this_thread::sleep_for(chrono::seconds(2));
    return id*2;
}

int may_throw_clean(int id)
{
    if (id == 13)
        throw runtime_error("Error");
    return id;
}

void may_throw(int id, exception_ptr& exc)
{
    try {
        if (id == 13)
            throw runtime_error("Error");
    }
    catch(...)
    {
        exc = current_exception();
    }
    return;// id;
}

int main()
{
    cout << "Hello Futures!" << endl;

    packaged_task<int(int)> pt(answer); // pt -> void(int)
    future<int> pt_res = pt.get_future();
    thread pt_thread(move(pt), 10);
    pt_thread.detach();
    // or simply call pt();

    cout << "PT res = " << pt_res.get() << endl;

    future<int> fres = async(launch::async, may_throw_clean, 13);
    cout << "After launching" << endl;
    fres.wait();
    try{
        cout << "Ans = " << fres.get() << endl;
    }
    catch(const runtime_error& err)
    {
        cout << "Error in future result! " << err.what() << endl;
    }

    exception_ptr excptr;
    scoped_thread th(may_throw, 13, ref(excptr));
    th.join();

    try
    {
        if(excptr)
            rethrow_exception(excptr);
    }
    catch(const runtime_error& err)
    {
        cout << "Error = " << err.what() << endl;
    }

    return 0;
}

