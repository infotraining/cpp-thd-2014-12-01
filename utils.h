#pragma once
#include <thread>
#include <string>
#include <unordered_map>
#include <typeindex>
#include <typeinfo>
#include <chrono>

class scoped_thread
{
    std::thread thd_;

public:

    scoped_thread(std::thread&& thd) : thd_(move(thd)) {}

    scoped_thread(const scoped_thread&) = delete;
    scoped_thread& operator=(const scoped_thread&) = delete;

    //scoped_thread(scoped_thread&& other) = default; doesnt work in MVC2013
    scoped_thread(scoped_thread&& other)
    {
        thd_ = move(other.thd_);
    }
    scoped_thread& operator=(scoped_thread&& other)
    {
        thd_ = move(other.thd_);
    }

    template <typename... T>
    scoped_thread(T&&... args) : thd_( std::forward<T>(args)... )
    {
    }

    void join()
    {
        thd_.join();
    }

    ~scoped_thread()
    {
        if(thd_.joinable())
            join();
    }
};


template<class Duration>
class Timer
{
    std::string msg_;
    std::chrono::time_point<std::chrono::high_resolution_clock> start;
    static std::unordered_map<std::type_index, std::string> suffix;
    // perhaps better to use boost::fusion map of types

public:
    Timer(const std::string& msg) : msg_(msg)
    {
        start = std::chrono::high_resolution_clock::now();
    }

    Timer(const Timer&) = delete;
    Timer& operator=(const Timer&) = delete;

    ~Timer()
    {
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << msg_ << ", duration: ";
        std::cout << std::chrono::duration_cast<Duration>(end-start).count();
        std::cout << suffix[std::type_index(typeid(Duration))] << std::endl;
    }
};

template <typename Duration>
std::unordered_map<std::type_index, std::string> Timer<Duration>::suffix =
{ {typeid(std::chrono::milliseconds), "ms"},
  {typeid(std::chrono::microseconds), "us"},
  {typeid(std::chrono::seconds), "s"},
  {typeid(std::chrono::nanoseconds), "ns"},
  {typeid(std::chrono::minutes), "min"},
  {typeid(std::chrono::hours), "h"}
};
