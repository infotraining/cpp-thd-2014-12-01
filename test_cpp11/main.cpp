#include <iostream>
#include <future>

using namespace std;

int main()
{
    future<int> ans = async(launch::async,
                            []() {return 42;});
    cout << ans.get() << endl;
    return 0;
}

